#include <Billboarding/Billboard.h>
#include <Billboarding/Export.h>

namespace ge {

	namespace gl {

		class Program;
	}

	namespace ad
	{
		class BILLBOARDING_EXPORT CPUBillboard : protected Billboard {

			public:
				// constructors
				CPUBillboard();

				// methods
				void recompute(const glm::mat4& view, glm::mat4& worldMatrix) override;

		};
	}
}