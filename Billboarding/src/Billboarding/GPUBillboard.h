#pragma once

#include <Billboarding/Billboard.h>
#include <Billboarding/Export.h>

#include <memory>
#include <string>

namespace ge {

	namespace gl {

		class Program;
	}

	namespace ad
	{
		class BILLBOARDING_EXPORT GPUBillboard : public Billboard {

		public:
			// shader
			static const std::string computeBillboardGLPosition;

			// constructors
			GPUBillboard(std::shared_ptr<ge::gl::Program> shaderProgram);

			// methods
			void recompute(const glm::mat4& view, glm::mat4& worldMatrix) override;

			// getters
			std::shared_ptr<ge::gl::Program> getShaderProgram();

			// setters
			void setShaderprogram(std::shared_ptr<ge::gl::Program> shaderProgram);

		private:
			std::shared_ptr<ge::gl::Program> shaderProgram;

		};
	}
}