#pragma once

#include <glm/glm.hpp>

namespace ge {

	namespace ad
	{
		class Billboard {

			public:
				// members
				enum MODE { BILLBOARD_POINT, BILLBOARD_AXIAL_X, BILLBOARD_AXIAL_Y, BILLBOARD_AXIAL_Z, BILLBOARD_ARBITRARY };

				// methods
				virtual void recompute(const glm::mat4& view, glm::mat4& worldMatrix) = 0;

				/// mode Get
				int getMode() { return mode; }

				/// mode & vector Set
				void setPointSprite(glm::mat4 view) { this->vector = glm::vec3(view[0][1], view[1][1], view[2][1]); this->mode = BILLBOARD_POINT; }
				void setAxialXYZ(int mode) {
					this->vector = glm::vec3(1, 0, 0);
					switch (mode) {
					case BILLBOARD_AXIAL_X:
						this->vector = glm::vec3(1, 0, 0);
						this->mode = BILLBOARD_AXIAL_X;
						break;
					case BILLBOARD_AXIAL_Y:
						this->vector = glm::vec3(0, 1, 0);
						this->mode = BILLBOARD_AXIAL_Y;
						break;
					case BILLBOARD_AXIAL_Z:
						this->vector = glm::vec3(0, 0, 1);
						this->mode = BILLBOARD_AXIAL_Z;
						break;
					}
				}
				void setArbitrary(glm::vec3 vector) { this->vector = vector; this->mode = BILLBOARD_ARBITRARY; }

			protected:
				// members
				int mode;
				glm::vec3 vector;

		};
	}

}