#include <Billboarding/CPUBillboard.h>

#include <geUtil/OrbitCamera.h>
#include <geUtil/PerspectiveCamera.h>
#include <glm/gtc/type_ptr.hpp>

ge::ad::CPUBillboard::CPUBillboard() {

	this->mode = BILLBOARD_AXIAL_Y;
	this->vector = glm::vec3(0, 1, 0);
}

void ge::ad::CPUBillboard::recompute(const glm::mat4& view, glm::mat4& worldMatrix) {

	glm::vec3 position = glm::vec3(worldMatrix[3][0], worldMatrix[3][1], worldMatrix[3][2]);
	worldMatrix[3][0] = 0; worldMatrix[3][1] = 0; worldMatrix[3][2] = 0;

	glm::vec3 upVector = vector;
	glm::vec3 rightVector;

	glm::mat4 invView = inverse(view);
	glm::vec3 eye_position = glm::vec3(invView[3][0], invView[3][1], invView[3][2]);

	glm::vec3 forwardVector = eye_position - position;

	switch (mode) {
	case 0: //BILLBOARD_POINT
		forwardVector = glm::normalize(forwardVector);
		rightVector = glm::cross(upVector, forwardVector);
		upVector = glm::cross(forwardVector, rightVector);
		break;
	case 1: //BILLBOARD_AXIAL_X
		forwardVector.x = 0;
		forwardVector = glm::normalize(forwardVector);
		rightVector = glm::cross(upVector, forwardVector);
		break;
	case 2: //BILLBOARD_AXIAL_Y
		forwardVector.y = 0;
		forwardVector = glm::normalize(forwardVector);
		rightVector = glm::cross(upVector, forwardVector);
		break;
	case 3: //BILLBOARD_AXIAL_Z
		forwardVector.z = 0;
		forwardVector = glm::normalize(forwardVector);
		rightVector = glm::cross(upVector, forwardVector);
		break;
	case 4: //BILLBOARD_ARBITRARY
		forwardVector = glm::normalize(forwardVector);
		upVector = glm::normalize(upVector);
		rightVector = glm::normalize(cross(upVector, forwardVector));
		forwardVector = glm::cross(rightVector, upVector);
	}

	glm::mat4 billboard;
	billboard[0] = glm::vec4(rightVector, 0.0f);
	billboard[1] = glm::vec4(upVector, 0.0f);
	billboard[2] = glm::vec4(forwardVector, 0.0f);
	billboard[3] = glm::vec4(position, 1.0f);

}