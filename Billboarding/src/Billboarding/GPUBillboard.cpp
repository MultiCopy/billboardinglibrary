#include <Billboarding/GPUBillboard.h>

#include <geGL/geGL.h>
#include <geUtil/OrbitCamera.h>
#include <geUtil/PerspectiveCamera.h>
#include <geGL/Program.h>
#include <glm/gtc/type_ptr.hpp>

const std::string ge::ad::GPUBillboard::computeBillboardGLPosition =
	R".(
		uniform uint mode;
		uniform vec3 vector;
		uniform vec3 billboard_position;

		vec4 computeBillboardPosition(vec3 position, mat4 model, mat4 view) {
			
			vec3 upVector = vector;
			vec3 rightVector;

			mat4 invView = inverse(view);
			vec3 eye_position = vec3(invView[3][0], invView[3][1], invView[3][2]);

			vec3 forwardVector = eye_position - billboard_position;

			switch (mode) {
				case 0: //BILLBOARD_POINT
					forwardVector = normalize(forwardVector);
					rightVector = cross(upVector, forwardVector);
					upVector = cross(forwardVector, rightVector);
					break;
				case 1: //BILLBOARD_AXIAL_X
					forwardVector.x = 0;
					forwardVector = normalize(forwardVector);
					rightVector = cross(upVector, forwardVector);
					break;
				case 2: //BILLBOARD_AXIAL_Y
					forwardVector.y = 0;
					forwardVector = normalize(forwardVector);
					rightVector = cross(upVector, forwardVector);
					break;
				case 3: //BILLBOARD_AXIAL_Z
					forwardVector.z = 0;
					forwardVector = normalize(forwardVector);
					rightVector = cross(upVector, forwardVector);
					break;
				case 4: //BILLBOARD_ARBITRARY
					forwardVector = normalize(forwardVector);
					upVector = normalize(upVector);
					rightVector = normalize(cross(upVector, forwardVector));
					forwardVector = cross(rightVector, upVector);
			}

			mat4 billboard;
			billboard[0] = vec4(rightVector, 0.0f);
			billboard[1] = vec4(upVector, 0.0f);
			billboard[2] = vec4(forwardVector, 0.0f);
			billboard[3] = vec4(billboard_position, 1.0f);
					
			return view * billboard * model * vec4(position, 1.0f);
		}
	).";

ge::ad::GPUBillboard::GPUBillboard(std::shared_ptr<ge::gl::Program> shaderProgram) {

	this->mode = BILLBOARD_AXIAL_Y;
	this->vector = glm::vec3(0, 1, 0);
	this->shaderProgram = shaderProgram;
}

void ge::ad::GPUBillboard::recompute(const glm::mat4& view, glm::mat4& worldMatrix) {

	shaderProgram->use();

	glm::vec3 position = glm::vec3(worldMatrix[3][0], worldMatrix[3][1], worldMatrix[3][2]);
	worldMatrix[3][0] = 0; worldMatrix[3][1] = 0; worldMatrix[3][2] = 0;

	shaderProgram
		->set1ui("mode", mode)
		->set3f("vector", vector.x, vector.y, vector.z)
		->set3f("billboard_position", position.x, position.y, position.z)
		->setMatrix4fv("model", glm::value_ptr(worldMatrix));
}

/* Getters & Setters */

/// ShaderProgram
std::shared_ptr<ge::gl::Program> ge::ad::GPUBillboard::getShaderProgram() { return shaderProgram; }
void ge::ad::GPUBillboard::setShaderprogram(std::shared_ptr<ge::gl::Program> shaderProgram) { this->shaderProgram = shaderProgram; }